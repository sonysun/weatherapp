//
//  WFGeocoderTests.m
//  weather
//
//  Created by Alex Nadein on 9/24/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WFGeocoder.h"

@interface WFGeocoderTests : XCTestCase

@end

@implementation WFGeocoderTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testIsBlockCalled
{
    WFGeocoder *geocoder = [[WFGeocoder alloc] init];
    __block BOOL blockFinished = NO;
    CGFloat testLatitude = 50.0042;
    CGFloat testLongitude = 36.2358;
    
    [geocoder reverseGeocodeWithLatitude:testLatitude longitude:testLongitude completionHandler:^(NSString *placeString)
    {
        blockFinished = YES;
        XCTAssertTrue(blockFinished);
    }];
    
    while (!blockFinished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

- (void)testDefinedPlaceName
{
    WFGeocoder *geocoder = [[WFGeocoder alloc] init];
    __block BOOL blockFinished = NO;
    CGFloat testLatitude = 50.0042;
    CGFloat testLongitude = 36.2358;
    NSString *testPlace = @"Kharkiv, Ukraine";
    
    [geocoder reverseGeocodeWithLatitude:testLatitude longitude:testLongitude completionHandler:^(NSString *placeString)
    {
        blockFinished = YES;
        XCTAssertEqualObjects(placeString, testPlace);
    }];
    
    while (!blockFinished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

@end
