//
//  WFLocationDetectorTests.m
//  weather
//
//  Created by Alex Nadein on 9/24/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WFLocationDetector.h"

@interface WFLocationDetectorTests : XCTestCase

@end

@implementation WFLocationDetectorTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testIsBlockCalled
{
    WFLocationDetector *locationDetector = [[WFLocationDetector alloc] init];
    __block BOOL blockFinished = NO;
    
    [locationDetector detectLocationWithBlock:^(WFLocation *location)
    {
        blockFinished = YES;
        XCTAssertTrue(blockFinished);
    }];

    while (!blockFinished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

@end
