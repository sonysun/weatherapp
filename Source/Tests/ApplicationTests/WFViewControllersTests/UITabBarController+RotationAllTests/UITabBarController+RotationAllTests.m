//
//  UITabBarController+RotationAllTests.m
//  weather
//
//  Created by Alex Nadein on 9/24/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface UITabBarController_RotationAllTests : XCTestCase

@end

@implementation UITabBarController_RotationAllTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testReturnsMaskForAllSupportedInterfaceOrientations
{
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    NSUInteger supportedOrientations = [tabBarController supportedInterfaceOrientations];
    XCTAssertEqual(supportedOrientations, UIInterfaceOrientationMaskAll, @"%lu must be equal to %lu", (unsigned long)supportedOrientations, (unsigned long)UIInterfaceOrientationMaskAll);
}

@end
