//
//  WFContentManagerTests.m
//  weather
//
//  Created by Alex Nadein on 9/24/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WFContentManager.h"

@interface WFContentManagerTests : XCTestCase

@end

@implementation WFContentManagerTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testIsBlockCalled
{
    WFContentManager *contentManager = [[WFContentManager alloc] init];
    __block BOOL blockFinished = NO;
    
    [contentManager getContentWithBlock:^(WFParsedForecast *lastestForecast)
    {
        blockFinished = YES;
        XCTAssertTrue(blockFinished);
    }];
    
    while (!blockFinished)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

@end
