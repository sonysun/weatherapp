//
//  WFForecastParserTests.m
//  weather
//
//  Created by Alex Nadein on 9/25/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WFForecastParser.h"

@interface WFForecastParserTests : XCTestCase

@end

@implementation WFForecastParserTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testParsingForecastFromDictionary
{
    NSString *testCurrentlyIcon = @"rain";
    NSString *testCurrentlySummary = @"Rainy all day long";
    NSNumber *testCurrentlyTemperature = [NSNumber numberWithFloat:17.52];
    
    NSNumber *testHourlyTemperature = [NSNumber numberWithFloat:15.22];
    NSString *testHourlySummary = @"Rainy";
    NSString *testHourlyIcon = @"rain-drops";
    NSNumber *testHourlyTime = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
    
    NSMutableDictionary *testHourlyData = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *testHourly = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *testCurrently = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *testForecast = [[NSMutableDictionary alloc] init];
    
    [testHourlyData setValue:testHourlyTemperature forKey:@"temperature"];
    [testHourlyData setValue:testHourlySummary forKey:@"summary"];
    [testHourlyData setValue:testHourlyIcon forKey:@"icon"];
    [testHourlyData setValue:testHourlyTime forKey:@"time"];
    
    [testHourly setValue:@[testHourlyData] forKey:@"data"];
    
    [testCurrently setValue:testCurrentlyIcon forKey:@"icon"];
    [testCurrently setValue:testCurrentlyTemperature forKey:@"temperature"];
    [testCurrently setValue:testCurrentlySummary forKey:@"summary"];
    
    [testForecast setValue:testCurrently forKey:@"currently"];
    [testForecast setValue:testHourly forKey:@"hourly"];
    
    WFParsedForecast *parsedForecast = [WFForecastParser parsedForecastObjectFromForecastDictionary:testForecast];
    
    XCTAssertEqualObjects([parsedForecast icon], testCurrentlyIcon);
    XCTAssertEqualObjects([parsedForecast summary], testCurrentlySummary);
    XCTAssertEqual([parsedForecast numberTemperature], [testCurrentlyTemperature integerValue]);
    NSString *testStringTemperature = [NSString stringWithFormat:@"%.f\u2103", [testCurrentlyTemperature floatValue]];
    XCTAssertEqualObjects([parsedForecast stringTemperature], testStringTemperature);
    
    for (WFParsedHourlyForecast *hourlyForecast in [parsedForecast hourlyForecastArray])
    {
        XCTAssertEqual([hourlyForecast icon], testHourlyIcon);
        XCTAssertEqual([hourlyForecast summary], testHourlySummary);
        XCTAssertEqualObjects([hourlyForecast date], [NSDate dateWithTimeIntervalSince1970:[testHourlyTime doubleValue]]);
        XCTAssertEqual([hourlyForecast temperature], [testHourlyTemperature integerValue]);
    }
}

@end
