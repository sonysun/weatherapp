//
//  WFLocation.h
//  weather
//
//  Created by Alex Nadein on 9/17/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WFLocation : NSObject

@property(nonatomic, copy) NSString *placeName;
@property(nonatomic, assign) float longitude;
@property(nonatomic, assign) float latitude;

@end
