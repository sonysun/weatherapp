//
//  WFLocationDetector.h
//  weather
//
//  Created by Alex Nadein on 9/17/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFLocation.h"

@interface WFLocationDetector : NSObject

- (void)detectLocationWithBlock:(void (^)(WFLocation *location))block;

@end
