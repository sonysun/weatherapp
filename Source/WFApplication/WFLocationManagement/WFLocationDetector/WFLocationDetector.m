//
//  WFLocationDetector.m
//  weather
//
//  Created by Alex Nadein on 9/17/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WFLocationDetector.h"
#import "WFLocationManager.h"
#import "WFGeocoder.h"
#import "WFDataStorage.h"

@interface WFLocationDetector ()

@property(nonatomic, strong) WFLocationManager *locationManager;
@property(nonatomic, strong) WFGeocoder *geocoder;
@property(nonatomic, strong) WFLocation *currentLocation;
@property(nonatomic, strong) WFDataStorage *dataStorage;

@end

@implementation WFLocationDetector

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        _currentLocation = [[WFLocation alloc] init];
        _locationManager = [[WFLocationManager alloc] initWithUpdateBlock:^(float latitude, float longitude)
        {
            [_currentLocation setLatitude:latitude];
            [_currentLocation setLongitude:longitude];
        }];
        
        _geocoder = [[WFGeocoder alloc] init];
    }
    
    return self;
}

- (void)detectLocationWithBlock:(void (^)(WFLocation *location))block
{
    [_geocoder reverseGeocodeWithLatitude:[_currentLocation latitude] longitude:[_currentLocation longitude] completionHandler:^(NSString *placeString)
    {
        [_currentLocation setPlaceName:placeString];
        block(_currentLocation);
    }];
}

@end
