//
//  WFGeocoder.h
//  weather
//
//  Created by Alex Nadein on 9/10/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface WFGeocoder : NSObject

- (void)reverseGeocodeWithLatitude:(float)latitude longitude:(float)longitude completionHandler:(void (^)(NSString *placeString))handlerBlock;

@end
