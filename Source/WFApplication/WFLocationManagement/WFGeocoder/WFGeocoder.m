//
//  WFGeocoder.m
//  weather
//
//  Created by Alex Nadein on 9/10/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFGeocoder.h"

@interface WFGeocoder ()

@property(nonatomic, strong) CLGeocoder *geocoder;

@end

@implementation WFGeocoder

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        _geocoder = [[CLGeocoder alloc] init];
    }
    
    return self;
}

- (void)reverseGeocodeWithLatitude:(float)latitude longitude:(float)longitude completionHandler:(void (^)(NSString *placeString))handlerBlock
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    [_geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
    {
        NSString *recentPlace = @"";
         
        if (error)
        {
            NSLog(@"Error finding place %@", error.description);
        }
        else
        {
            CLPlacemark *placemark = [placemarks lastObject];
            NSString *country = [placemark country];
            NSString *city = [placemark locality];
            recentPlace = [NSString stringWithFormat:@"%@, %@", city, country];
            NSLog(@"Now you are in %@", recentPlace);
             
            handlerBlock(recentPlace);
        }
    }];
}

@end
