//
//  WFLocationManager.h
//  weather
//
//  Created by Alex Nadein on 9/10/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>

@interface WFLocationManager : NSObject<CLLocationManagerDelegate>

- (instancetype)initWithUpdateBlock:(void (^)(float latitude, float longitude))block;

@end
