//
//  WFLocationManager.m
//  weather
//
//  Created by Alex Nadein on 9/10/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFLocationManager.h"
#import <Foundation/Foundation.h>

@interface WFLocationManager ()

@property(nonatomic, strong) CLLocationManager *locationManager;
@property(nonatomic, copy) void (^updateBlock)(float latitude, float longitude);

@end

@implementation WFLocationManager

- (instancetype)initWithUpdateBlock:(void (^)(float latitude, float longitude))block
{
    self = [super init];
    
    if (self != nil)
    {
        [self setUpdateBlock:block];
        
        _locationManager = [[CLLocationManager alloc] init];
        [_locationManager setDelegate:self];
        [_locationManager setDistanceFilter:kCLDistanceFilterNone];
        [_locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
        
        if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [_locationManager requestWhenInUseAuthorization];
        }
        
        [_locationManager startUpdatingLocation];
    }
    
    return self;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"New Location!");
    _updateBlock([[locations lastObject] coordinate].latitude, [[locations lastObject] coordinate].longitude);
}

@end
