//
//  WFParsedForecast.h
//  weather
//
//  Created by Alex Nadein on 9/14/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WFParsedHourlyForecast.h"

@interface WFParsedForecast : NSObject

@property(nonatomic, copy) NSString *summary;
@property(nonatomic, copy) NSString *place;
@property(nonatomic, copy) NSString *icon;
@property(nonatomic, copy) NSString *stringTemperature;
@property(nonatomic, assign) NSInteger numberTemperature;
@property(nonatomic, copy) NSArray *hourlyForecastArray;

@end
