//
//  WFParsedHourlyForecast.h
//  weather
//
//  Created by Alex Nadein on 9/16/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WFParsedHourlyForecast : NSObject

@property(nonatomic, copy) NSString *summary;
@property(nonatomic, copy) NSString *icon;
@property(nonatomic, copy) NSDate *date;
@property(nonatomic, assign) NSInteger temperature;

@end
