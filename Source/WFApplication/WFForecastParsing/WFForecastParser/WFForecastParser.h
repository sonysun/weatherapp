//
//  WFForecastParser.h
//  weather
//
//  Created by Alex Nadein on 9/14/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WFParsedForecast.h"

@interface WFForecastParser : NSObject

+ (WFParsedForecast *)parsedForecastObjectFromForecastDictionary:(NSDictionary *)forecastDictionary;

@end
