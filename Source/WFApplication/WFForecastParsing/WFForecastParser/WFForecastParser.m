//
//  WFForecastParser.m
//  weather
//
//  Created by Alex Nadein on 9/14/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFForecastParser.h"

@interface WFForecastParser ()

+ (NSString *)parseIconFromForecastDictionary:(NSDictionary *)forecastDictionary;
+ (NSString *)parseTemperatureFromForecastDictionary:(NSDictionary *)forecastDictionary;
+ (NSString *)parseWeatherSummaryFromForecastDictionary:(NSDictionary *)forecastDictionary;
+ (NSMutableArray *)parseHourlyForecastFromForecastDictionary:(NSDictionary *)forecastDictionary;

@end

@implementation WFForecastParser

#pragma mark - private methods

+ (NSString *)parseIconFromForecastDictionary:(NSDictionary *)forecastDictionary
{
    return [[forecastDictionary objectForKey:@"currently"] objectForKey:@"icon"];
}

+ (NSString *)parseTemperatureFromForecastDictionary:(NSDictionary *)forecastDictionary
{
    return [NSString stringWithFormat:@"%.f\u2103", [[[forecastDictionary objectForKey:@"currently"] objectForKey:@"temperature"] floatValue]];
}

+ (NSString *)parseWeatherSummaryFromForecastDictionary:(NSDictionary *)forecastDictionary
{
    return [[forecastDictionary objectForKey:@"currently"] objectForKey:@"summary"];
}

+ (NSArray *)parseHourlyForecastFromForecastDictionary:(NSDictionary *)forecastDictionary
{
    NSMutableArray *parsedHourlyForecasts = [[NSMutableArray alloc] init];
    
    for (NSDictionary *item in [[forecastDictionary objectForKey:@"hourly"] objectForKey:@"data"])
    {
        WFParsedHourlyForecast *hourlyForecast = [[WFParsedHourlyForecast alloc] init];
        [hourlyForecast setTemperature:[[item objectForKey:@"temperature"] integerValue]];
        [hourlyForecast setSummary:[item objectForKey:@"summary"]];
        [hourlyForecast setIcon:[item objectForKey:@"icon"]];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[[item objectForKey:@"time"] doubleValue]];
        [hourlyForecast setDate:date];
        [parsedHourlyForecasts addObject:hourlyForecast];
    }
    
    return parsedHourlyForecasts;
}

#pragma mark - public method

+ (WFParsedForecast *)parsedForecastObjectFromForecastDictionary:(NSDictionary *)forecastDictionary
{
    WFParsedForecast *parsedForecast = [[WFParsedForecast alloc] init];
    [parsedForecast setIcon:[WFForecastParser parseIconFromForecastDictionary:forecastDictionary]];
    [parsedForecast setStringTemperature:[WFForecastParser parseTemperatureFromForecastDictionary:forecastDictionary]];
    NSInteger temp = [[[forecastDictionary objectForKey:@"currently"] objectForKey:@"temperature"] integerValue];
    [parsedForecast setNumberTemperature:temp];
    [parsedForecast setSummary:[WFForecastParser parseWeatherSummaryFromForecastDictionary:forecastDictionary]];
    [parsedForecast setHourlyForecastArray:[WFForecastParser parseHourlyForecastFromForecastDictionary:forecastDictionary]];
    return parsedForecast;
}

@end
