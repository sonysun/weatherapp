//
//  WFSQLiteDatabaseManager.h
//  weather
//
//  Created by Alex Nadein on 9/18/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WFDAOProtocol.h"

@interface WFSQLiteDatabaseManager : NSObject<WFDAOProtocol>

@end
