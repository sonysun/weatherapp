//
//  WFSQLiteDatabaseManager.m
//  weather
//
//  Created by Alex Nadein on 9/18/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFSQLiteDatabaseManager.h"
#import "WFParsedForecast.h"
#import "WFLocation.h"
#import "FMDB.h"

NSTimeInterval const WEEKINTERVAL = -604800;

@interface WFSQLiteDatabaseManager ()

@property(nonatomic, strong) FMDatabase *database;

@end

@implementation WFSQLiteDatabaseManager

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *path = [[NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES)lastObject] stringByAppendingPathComponent:@"weather2.sqlite"];
        NSString *pathToSourceDB = [[NSBundle mainBundle] pathForResource:@"weather2" ofType:@"sqlite"];
        
        if (![fileManager fileExistsAtPath:path])
        {
            if ([fileManager isReadableFileAtPath:pathToSourceDB])
            {
                NSError *error = nil;
                [fileManager copyItemAtPath:pathToSourceDB toPath:path error:&error];
                
                BOOL success = [[NSURL URLWithString:path] setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
                
                if (!success)
                {
                    NSLog(@"Error excluding %@ from backup %@", [[NSURL URLWithString:path] lastPathComponent], error);
                }
            }
            else
            {
                NSLog(@"ERROR: copying source DB is failed!");
            }
        }

        _database = [FMDatabase databaseWithPath:path];
    }
    
    return self;
}

#pragma mark - <WFDAOProtocol> implementation

- (void)saveForecast:(WFParsedForecast *)parsedForecast forLocation:(WFLocation *)location
{
    [_database open];
    
    [_database executeUpdateWithFormat:@"INSERT INTO forecast (date, latitude, longitude) VALUES (%@, %f, %f)", [NSDate date], [location latitude], [location longitude]];
    
    [_database close];
}

- (void)updateForecast:(WFParsedForecast *)parsedForecast
{
    [_database open];
    
    [_database executeUpdateWithFormat:@"UPDATE forecast SET icon = %@, summary = %@, temperature = %@ WHERE date = (SELECT max(date) FROM forecast)", [parsedForecast icon], [parsedForecast summary], [NSNumber numberWithInteger:[parsedForecast numberTemperature]]];
    
    FMResultSet *result = [_database executeQuery:@"SELECT id FROM forecast WHERE date = (SELECT max(date) FROM forecast)"];

    if ([result next])
    {
        int forecastID = [result intForColumn:@"id"];
        
        for (WFParsedHourlyForecast *forecast in [parsedForecast hourlyForecastArray])
        {
            [_database executeUpdateWithFormat:@"INSERT INTO hourlyforecast (date, icon, summary, temperature, forecast_id) VALUES (%@, %@, %@, %@, %i)", [forecast date], [forecast icon], [forecast summary], [NSNumber numberWithInteger:[forecast temperature]], forecastID];
        }
    }
    
    [_database close];
}

- (void)updateForecastPlace:(NSString *)placeString
{
    [_database open];
    
    [_database executeUpdateWithFormat:@"UPDATE forecast SET place = %@ WHERE date = (SELECT max(date) FROM forecast)", placeString];
    
    [_database close];
}

- (WFParsedForecast *)lastestForecast
{
    [_database open];
    
    WFParsedForecast *lastestForecast = [[WFParsedForecast alloc] init];
    FMResultSet *result = [_database executeQueryWithFormat:@"SELECT * FROM forecast WHERE date = (SELECT max(date) FROM forecast)"];
    
    if ([result next])
    {
        int forecastID = [result intForColumn:@"id"];
        
        [lastestForecast setPlace:[result stringForColumn:@"place"]];
        [lastestForecast setSummary:[result stringForColumn:@"summary"]];
        [lastestForecast setIcon:[result stringForColumn:@"icon"]];
        [lastestForecast setNumberTemperature:[result intForColumn:@"temperature"]];
        [lastestForecast setStringTemperature:[NSString stringWithFormat:@"%i\u2103", [result intForColumn:@"temperature"]]];
        
        result = [_database executeQueryWithFormat:@"SELECT * from hourlyforecast WHERE forecast_id = %i", forecastID];
        NSMutableArray *parsedHourlyForecasts = [[NSMutableArray alloc] init];
        
        while ([result next])
        {
            WFParsedHourlyForecast *forecast = [[WFParsedHourlyForecast alloc] init];
            [forecast setTemperature:[result intForColumn:@"temperature"]];
            [forecast setSummary:[result stringForColumn:@"summary"]];
            [forecast setIcon:[result stringForColumn:@"icon"]];
            [forecast setDate:[result dateForColumn:@"date"]];
            [parsedHourlyForecasts addObject:forecast];
        }
        
        [lastestForecast setHourlyForecastArray:parsedHourlyForecasts];
    }
    
    [_database close];
    return lastestForecast;
}

- (NSArray *)forecastsForTheLastWeek
{
    [_database open];
    
    FMResultSet *result = [_database executeQueryWithFormat:@"SELECT * FROM forecast WHERE date > %@", [NSDate dateWithTimeIntervalSinceNow:WEEKINTERVAL]];
    NSMutableArray *forecastsArray = [[NSMutableArray alloc] init];
    
    while ([result next])
    {
        WFParsedForecast *forecast = [[WFParsedForecast alloc] init];
        int forecastID = [result intForColumn:@"id"];
        
        [forecast setPlace:[result stringForColumn:@"place"]];
        [forecast setSummary:[result stringForColumn:@"summary"]];
        [forecast setIcon:[result stringForColumn:@"icon"]];
        [forecast setNumberTemperature:[result intForColumn:@"temperature"]];
        [forecast setStringTemperature:[NSString stringWithFormat:@"%i\u2103", [result intForColumn:@"temperature"]]];
        
        FMResultSet *resultSet = [_database executeQueryWithFormat:@"SELECT * from hourlyforecast WHERE forecast_id = %i", forecastID];
        NSMutableArray *parsedHourlyForecasts = [[NSMutableArray alloc] init];
        
        while ([resultSet next])
        {
            WFParsedHourlyForecast *forecast = [[WFParsedHourlyForecast alloc] init];
            [forecast setTemperature:[result intForColumn:@"temperature"]];
            [forecast setSummary:[result stringForColumn:@"summary"]];
            [forecast setIcon:[result stringForColumn:@"icon"]];
            [forecast setDate:[result dateForColumn:@"date"]];
            [parsedHourlyForecasts addObject:forecast];
        }
        
        [forecast setHourlyForecastArray:parsedHourlyForecasts];
        [forecastsArray addObject:forecast];
    }
    
    [_database close];
    return forecastsArray;
}

@end
