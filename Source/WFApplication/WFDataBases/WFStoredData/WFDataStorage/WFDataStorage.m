//
//  WFDataStorage.m
//  weather
//
//  Created by Alex Nadein on 9/11/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFDataStorage.h"
#import <CoreData/CoreData.h>
#import "WFForecast+CoreDataProperties.h"
#import "WFHourlyForecast+CoreDataProperties.h"

NSString *const FORECAST_ENTITY_NAME = @"WFForecast";
NSString *const HOURLY_ENTITY_NAME = @"WFHourlyForecast";
NSTimeInterval const WEEK = -604800;

@interface WFDataStorage ()

@property(nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property(nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSString *)applicationDocumentsDirectory;
- (WFParsedForecast *)parsedForecastFromManagedObject:(WFForecast *)forecast;

@end

@implementation WFDataStorage

#pragma mark - storage management

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
   
    if (coordinator != nil)
    {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil)
    {
        return _managedObjectModel;
    }
    
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil)
    {
        return _persistentStoreCoordinator;
    }

    NSURL *storeUrl = [NSURL fileURLWithPath:[[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"weather.sqlite"]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error])
    {
        /*Error for store creation should be handled in here*/
    }
    
    return _persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - <WFDAOProtocol> implementation

- (void)saveForecast:(WFParsedForecast *)parsedForecast forLocation:(WFLocation *)location
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    WFForecast *forecast = [[WFForecast alloc] initWithEntity:[NSEntityDescription entityForName:FORECAST_ENTITY_NAME inManagedObjectContext:context] insertIntoManagedObjectContext:context];
    
    [forecast setDate:[NSDate date]];
    [forecast setLongitude:[NSNumber numberWithDouble:[location longitude]]];
    [forecast setLatitude:[NSNumber numberWithDouble:[location latitude]]];
    
    NSError *error = nil;
    
    if ([context save:&error])
    {
        NSLog(@"New forecast information was saved!");
    }
    else
    {
        NSLog(@"The forecast information was not saved: %@", [error userInfo]);
    }
}

- (void)updateForecast:(WFParsedForecast *)parsedForecast
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:FORECAST_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:fetchRequest error:&error];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@.@max.date", fetchedArray];
    WFForecast *forecastToUpdate = [[fetchedArray filteredArrayUsingPredicate:predicate] firstObject];
    
    [forecastToUpdate setTemperature:[NSNumber numberWithInteger:[parsedForecast numberTemperature]]];
    [forecastToUpdate setSummary:[parsedForecast summary]];
    [forecastToUpdate setIcon:[parsedForecast icon]];
    
    for (WFParsedHourlyForecast *item in [parsedForecast hourlyForecastArray])
    {
        WFHourlyForecast *forecast = [[WFHourlyForecast alloc] initWithEntity:[NSEntityDescription entityForName:HOURLY_ENTITY_NAME inManagedObjectContext:context] insertIntoManagedObjectContext:context];
        
        [forecast setTemperature:[NSNumber numberWithInteger:[item temperature]]];
        [forecast setSummary:[item summary]];
        [forecast setDate:[item date]];
        [forecast setIcon:[item icon]];
        [forecastToUpdate addHourlyObject:forecast];
    }
    
    error = nil;
    
    if ([context save:&error])
    {
        NSLog(@"Forecast information was updated!");
    }
    else
    {
        NSLog(@"The forecast information was not updated: %@", [error userInfo]);
    }
}

- (void)updateForecastPlace:(NSString *)placeString
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:FORECAST_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:fetchRequest error:&error];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@.@max.date", fetchedArray];
    WFForecast *forecastToUpdate = [[fetchedArray filteredArrayUsingPredicate:predicate] firstObject];
    
    [forecastToUpdate setPlace:placeString];
    error = nil;
    
    if ([context save:&error])
    {
        NSLog(@"Forecast information was updated!");
    }
    else
    {
        NSLog(@"The forecast information was not updated: %@", [error userInfo]);
    }
}

- (WFParsedForecast *)lastestForecast
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:FORECAST_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
   
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:fetchRequest error:&error];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@.@max.date", fetchedArray];
    WFForecast *lastestForecast = [[fetchedArray filteredArrayUsingPredicate:predicate] firstObject];

    WFParsedForecast *parsedForecast = [self parsedForecastFromManagedObject:lastestForecast];
    return parsedForecast;
}

- (NSArray *)forecastsForTheLastWeek
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:FORECAST_ENTITY_NAME inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedArray = [context executeFetchRequest:fetchRequest error:&error];
    NSDate *weekAgo = [NSDate dateWithTimeIntervalSinceNow:WEEK];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date >= %@", weekAgo];
    NSArray *weekForecasts = [fetchedArray filteredArrayUsingPredicate:predicate];
    
    NSMutableArray *parsedWeekForecasts = [[NSMutableArray alloc] init];
    
    for (WFForecast *forecast in weekForecasts)
    {
        [parsedWeekForecasts addObject:[self parsedForecastFromManagedObject:forecast]];
    }
    
    return parsedWeekForecasts;
}

- (WFParsedForecast *)parsedForecastFromManagedObject:(WFForecast *)forecast
{
    WFParsedForecast *parsedForecast = [[WFParsedForecast alloc] init];
    [parsedForecast setPlace:[forecast place]];
    [parsedForecast setSummary:[forecast summary]];
    [parsedForecast setIcon:[forecast icon]];
    [parsedForecast setNumberTemperature:[[forecast temperature] integerValue]];
    [parsedForecast setStringTemperature:[NSString stringWithFormat:@"%.f\u2103", [[forecast temperature] floatValue]]];
    NSArray *hourlyForecasts = [[forecast hourly] allObjects];
    NSMutableArray *parsedHourlyForecasts = [[NSMutableArray alloc] init];
    
    for (WFHourlyForecast *forecast in hourlyForecasts)
    {
        WFParsedHourlyForecast *hourlyForecast = [[WFParsedHourlyForecast alloc] init];
        [hourlyForecast setTemperature:[[forecast temperature] integerValue]];
        [hourlyForecast setSummary:[forecast summary]];
        [hourlyForecast setIcon:[forecast icon]];
        [hourlyForecast setDate:[forecast date]];
        [parsedHourlyForecasts addObject:hourlyForecast];
    }
    
    [parsedForecast setHourlyForecastArray:parsedHourlyForecasts];
    
    return parsedForecast;
}

@end
