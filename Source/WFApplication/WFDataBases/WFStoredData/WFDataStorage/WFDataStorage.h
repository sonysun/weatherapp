//
//  WFDataStorage.h
//  weather
//
//  Created by Alex Nadein on 9/11/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "WFParsedForecast.h"
#import "WFLocation.h"
#import "WFDAOProtocol.h"

@interface WFDataStorage : NSObject<WFDAOProtocol>

@end
