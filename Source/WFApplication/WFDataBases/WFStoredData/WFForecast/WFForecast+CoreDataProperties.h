//
//  WFForecast+CoreDataProperties.h
//
//
//  Created by Alex Nadein on 9/16/15.
//
//
//  Delete this file and regenerate it using "Create NSManagedObject Subclass…"
//  to keep your implementation up to date with your model.
//

#import "WFForecast.h"

NS_ASSUME_NONNULL_BEGIN

@interface WFForecast (CoreDataProperties)

@property(nullable, nonatomic, retain) NSDate *date;
@property(nullable, nonatomic, retain) NSString *icon;
@property(nullable, nonatomic, retain) NSNumber *latitude;
@property(nullable, nonatomic, retain) NSNumber *longitude;
@property(nullable, nonatomic, retain) NSString *place;
@property(nullable, nonatomic, retain) NSString *summary;
@property(nullable, nonatomic, retain) NSNumber *temperature;
@property(nullable, nonatomic, retain) NSSet<WFHourlyForecast *> *hourly;

@end

@interface WFForecast (CoreDataGeneratedAccessors)

- (void)addHourlyObject:(WFHourlyForecast *)value;
- (void)removeHourlyObject:(WFHourlyForecast *)value;
- (void)addHourly:(NSSet<WFHourlyForecast *> *)values;
- (void)removeHourly:(NSSet<WFHourlyForecast *> *)values;

@end

NS_ASSUME_NONNULL_END
