//
//  WFHourlyForecast.h
//
//
//  Created by Alex Nadein on 9/16/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class WFForecast;

NS_ASSUME_NONNULL_BEGIN

@interface WFHourlyForecast : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "WFHourlyForecast+CoreDataProperties.h"
