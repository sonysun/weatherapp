//
//  WFHourlyForecast+CoreDataProperties.h
//
//
//  Created by Alex Nadein on 9/16/15.
//
//
//  Delete this file and regenerate it using "Create NSManagedObject Subclass…"
//  to keep your implementation up to date with your model.
//

#import "WFHourlyForecast.h"

NS_ASSUME_NONNULL_BEGIN

@interface WFHourlyForecast (CoreDataProperties)

@property(nullable, nonatomic, retain) NSString *summary;
@property(nullable, nonatomic, retain) NSNumber *temperature;
@property(nullable, nonatomic, retain) NSDate *date;
@property(nullable, nonatomic, retain) NSString *icon;
@property(nullable, nonatomic, retain) WFForecast *current;

@end

NS_ASSUME_NONNULL_END
