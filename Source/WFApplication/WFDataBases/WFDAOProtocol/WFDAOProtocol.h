//
//  WFDAOProtocol.h
//  weather
//
//  Created by Alex Nadein on 9/18/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//
@class WFParsedForecast;
@class WFLocation;

@protocol WFDAOProtocol<NSObject>

- (void)saveForecast:(WFParsedForecast *)parsedForecast forLocation:(WFLocation *)location;
- (void)updateForecast:(WFParsedForecast *)parsedForecast;
- (void)updateForecastPlace:(NSString *)placeString;
- (WFParsedForecast *)lastestForecast;
- (NSArray *)forecastsForTheLastWeek;

@end