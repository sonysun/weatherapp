//
//  WFForecastCell.h
//  weather
//
//  Created by Alex Nadein on 9/15/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WFForecastCell : UITableViewCell

@property(nonatomic, weak) IBOutlet UILabel *temperatureLabel;
@property(nonatomic, weak) IBOutlet UILabel *summaryLabel;
@property(nonatomic, weak) IBOutlet UIImageView *iconImageView;

@end
