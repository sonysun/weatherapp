//
//  WFWeatherForecaster.m
//  weather
//
//  Created by Alex Nadein on 9/9/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFWeatherForecaster.h"
#import "WFForecastParser.h"
#import "AFHTTPRequestOperationManager.h"

NSString *const URL = @"https://api.forecast.io/forecast/5971c567e02ffe4a4759f0bcd774bcf9/";
NSString *const UNITS_PARAMETER = @"?units=si";

@interface WFWeatherForecaster ()

@property(nonatomic, strong) AFHTTPRequestOperationManager *manager;

@end

@implementation WFWeatherForecaster

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        [self setManager:[AFHTTPRequestOperationManager manager]];
    }
    
    return self;
}

- (void)weatherForecastForLocation:(WFLocation *)location completionHandler:(void (^)(WFParsedForecast *forecast))handlerBlock
{
    CGFloat latitude = [location latitude];
    CGFloat longitude = [location longitude];
    NSString *locationString = [NSString stringWithFormat:@"%.6f,%.6f", latitude, longitude];
    
    NSMutableString *requestUrl = [[NSMutableString alloc] initWithString:URL];
    [requestUrl appendString:locationString];
    [requestUrl appendString:UNITS_PARAMETER];
    
    void (^successBlock)(AFHTTPRequestOperation *operation, id responseObject) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        WFParsedForecast *parsedForecast = [WFForecastParser parsedForecastObjectFromForecastDictionary:responseObject];
        
        handlerBlock(parsedForecast);
        
        //         [_dataStorage updateForecast:parsedForecast];
        //         [self setForecast:parsedForecast];
        //         [_tableView reloadData];
        //         [[self refreshControl] endRefreshing];
    };
   
    void (^failureBlock)(AFHTTPRequestOperation *operation, NSError *error) = ^(AFHTTPRequestOperation *operation, NSError *error)
    {
        NSLog(@"API Call Error: %@", error);
    };
    
    [_manager GET:requestUrl parameters:nil success:successBlock failure:failureBlock];
}

@end
