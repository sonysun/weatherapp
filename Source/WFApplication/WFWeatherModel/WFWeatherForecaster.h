//
//  WFWeatherForecaster.h
//  weather
//
//  Created by Alex Nadein on 9/9/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "WFLocation.h"
@class WFParsedForecast;

@interface WFWeatherForecaster : NSObject

- (void)weatherForecastForLocation:(WFLocation *)location completionHandler:(void (^)(WFParsedForecast *forecast))handlerBlock;

@end
