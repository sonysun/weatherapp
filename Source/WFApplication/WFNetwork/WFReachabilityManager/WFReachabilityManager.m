//
//  WFReachabilityManager.m
//  weather
//
//  Created by Alex Nadein on 9/18/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFReachabilityManager.h"

NSString *const REMOTE_HOST = @"api.forecast.io";

@interface WFReachabilityManager ()

@property(nonatomic, strong) WFReachability *hostReachability;

@end

@implementation WFReachabilityManager

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
        _hostReachability = [WFReachability reachabilityWithHostName:REMOTE_HOST];
        [_hostReachability startNotifier];
    }
    
    return self;
}

- (void)reachabilityChanged:(NSNotification *)note
{
    WFReachability *curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[WFReachability class]]);
    _netStatus = [curReach currentReachabilityStatus];
}
   
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end
