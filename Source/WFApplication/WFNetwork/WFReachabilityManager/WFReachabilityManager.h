//
//  WFReachabilityManager.h
//  weather
//
//  Created by Alex Nadein on 9/18/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WFReachability.h"

@interface WFReachabilityManager : NSObject

@property(nonatomic, assign) NetworkStatus netStatus;

@end
