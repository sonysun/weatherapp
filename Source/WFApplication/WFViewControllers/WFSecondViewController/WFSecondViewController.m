//
//  SecondViewController.m
//  weather
//
//  Created by Alex Nadein on 9/9/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFSecondViewController.h"
#import "WFDataStorage.h"
#import "WFHourlyCell.h"
#import "WFSQLiteDatabaseManager.h"

@interface WFSecondViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, weak) IBOutlet UINavigationItem *titleView;
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) UIRefreshControl *refreshControl;
@property(nonatomic, copy) NSMutableDictionary *sectionsDictionary;
@property(nonatomic, copy) NSArray *sectionsTitlesArray;
@property(nonatomic, strong) id<WFDAOProtocol> dataStorage;

- (void)tableUpdate;
- (NSMutableDictionary *)createSectionsDictionaryFromHourlyForecastDataArray:(NSArray *)hourlyForecastArray;
- (NSMutableDictionary *)sortForecastsByDateInSectionsDictionary:(NSMutableDictionary *)sectionsDictionary;
- (float)maxTemperatureForTheLastWeek;

@end

@implementation WFSecondViewController

#pragma mark - button actions

- (IBAction)maxTempButtonPushed:(id)sender
{
    NSString *message = [NSString stringWithFormat:@"Max temperature for the last week is %.f", [self maxTemperatureForTheLastWeek]];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Max temperature"
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
                                          
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction *action) {}];
                                          
    [alertController addAction:defaultAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_sectionsTitlesArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [_sectionsTitlesArray objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionTitle = [_sectionsTitlesArray objectAtIndex:section];
    NSArray *sections = [_sectionsDictionary objectForKey:sectionTitle];
    return [sections count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TableItem";
    WFHourlyCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSString *sectionTitle = [_sectionsTitlesArray objectAtIndex:[indexPath section]];
    NSArray *sectionItems = [_sectionsDictionary objectForKey:sectionTitle];
    WFParsedHourlyForecast *item = [sectionItems objectAtIndex:[indexPath row]];
    
    return [self configureCell:cell withDataFromObject:item];
}

- (WFHourlyCell *)configureCell:(WFHourlyCell *)cell withDataFromObject:(WFParsedHourlyForecast *)item
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSString *timeAndTemperature = [NSString stringWithFormat:@"%@  %ld\u2103", [formatter stringFromDate:[item date]], (long)[item temperature]];
    [[cell temperatureLabel] setText:timeAndTemperature];
    [[cell summaryLabel] setText:[item summary]];
    [[cell summaryLabel] setFont:[UIFont fontWithName:@"Helvetica" size:15]];
    [[cell summaryLabel] setTextAlignment:NSTextAlignmentCenter];
    [[cell iconImageView] setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [item icon]]]];
    
    return cell;
}

#pragma mark - hourlyForecastArray management

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_titleView setTitle:@"Pull down to update"];
    
    _dataStorage = [[WFSQLiteDatabaseManager alloc] init];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl setBackgroundColor:[UIColor blueColor]];
    [_refreshControl setTintColor:[UIColor whiteColor]];
    [_refreshControl addTarget:self action:@selector(tableUpdate) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:_refreshControl];
    
    [self tableUpdate];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self tableUpdate];
}

- (void)tableUpdate;
{
    NSLog(@"Update hourly table!");
    WFParsedForecast *lastestForecast = [_dataStorage lastestForecast];
    [_titleView setTitle:[lastestForecast place]];
    NSMutableDictionary *tempDictionary = [self createSectionsDictionaryFromHourlyForecastDataArray:[lastestForecast hourlyForecastArray]];
    _sectionsDictionary = [self sortForecastsByDateInSectionsDictionary:tempDictionary];
    _sectionsTitlesArray = [[_sectionsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [_tableView reloadData];
    [[self refreshControl] endRefreshing];
}

- (NSMutableDictionary *)createSectionsDictionaryFromHourlyForecastDataArray:(NSArray *)hourlyForecastArray
{
    NSMutableDictionary *sectionsDictionary = [[NSMutableDictionary alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM dd"];
    
    for (WFParsedHourlyForecast *forecast in hourlyForecastArray)
    {
        NSMutableArray *categoryArray = [NSMutableArray arrayWithArray:[sectionsDictionary objectForKey:[formatter stringFromDate:forecast.date]]];
        
        if (categoryArray == nil)
        {
            categoryArray = [[NSMutableArray alloc] init];
        }
        
        [categoryArray addObject:forecast];
        [sectionsDictionary setObject:[categoryArray copy] forKey:[formatter stringFromDate:forecast.date]];
    }

    return sectionsDictionary;
}

- (NSMutableDictionary *)sortForecastsByDateInSectionsDictionary:(NSMutableDictionary *)sectionsDictionary
{
    NSMutableDictionary *sortedSectionsDictionary = [[NSMutableDictionary alloc] init];
    
    for (id key in sectionsDictionary)
    {
        NSArray *forecastsArray = [sectionsDictionary objectForKey:key];
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
        NSArray *sortDescriptors = @[sortDescriptor];
        [sortedSectionsDictionary setObject:[forecastsArray sortedArrayUsingDescriptors:sortDescriptors] forKey:key];
    }
    
    return sortedSectionsDictionary;
}

- (float)maxTemperatureForTheLastWeek
{
    NSArray *weekForecasts = [_dataStorage forecastsForTheLastWeek];
    return [[weekForecasts valueForKeyPath:@"@max.numberTemperature"] floatValue];
}

@end
