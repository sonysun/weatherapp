//
//  FirstViewController.m
//  weather
//
//  Created by Alex Nadein on 9/9/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFFirstViewController.h"
#import "WFForecastParser.h"
#import "WFForecastCell.h"
#import "WFLocation.h"
#import "WFContentManager.h"

@interface WFFirstViewController ()<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) UIRefreshControl *refreshControl;
@property(nonatomic, weak) IBOutlet UINavigationItem *navigationTitle;
@property(nonatomic, weak) IBOutlet UITableView *tableView;
@property(nonatomic, strong) WFParsedForecast *forecast;
@property(nonatomic, strong) WFContentManager *contentManager;
@property(nonatomic, strong) NSUserDefaults *sharedDefaults;

- (void)showLatestForecast;
- (WFForecastCell *)configureCell:(WFForecastCell *)cell;

@end

@implementation WFFirstViewController

#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    WFForecastCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    return [self configureCell:cell];
}

- (WFForecastCell *)configureCell:(WFForecastCell *)cell
{
    [[cell temperatureLabel] setText:[_forecast stringTemperature]];
    [[cell temperatureLabel] setFont:[UIFont fontWithName:@"Helvetica" size:90]];
    [[cell temperatureLabel] setTextAlignment:NSTextAlignmentLeft];
    [[cell summaryLabel] setText:[_forecast summary]];
    [[cell summaryLabel] setFont:[UIFont fontWithName:@"Helvetica" size:20]];
    [[cell summaryLabel] setTextAlignment:NSTextAlignmentCenter];
    UIImage *tempImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [_forecast icon]]];
    [[cell iconImageView] setImage:tempImage];
    return cell;
}

#pragma mark - WFForecast management

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _contentManager = [[WFContentManager alloc] init];
    
    _sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.nix.weatherApp"];
    
    [_navigationTitle setTitle:@"Pull down to update"];
    _tableView.separatorColor = [UIColor clearColor];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl setBackgroundColor:[UIColor blueColor]];
    [_refreshControl setTintColor:[UIColor whiteColor]];
    [_refreshControl addTarget:self action:@selector(showLatestForecast) forControlEvents:UIControlEventValueChanged];
    [_tableView addSubview:_refreshControl];
    
    [self showLatestForecast];
}

- (void)showLatestForecast
{
    [_contentManager getContentWithBlock:^(WFParsedForecast *lastestForecast)
    {
        [_navigationTitle setTitle:[lastestForecast place]];
        [self setForecast:lastestForecast];
        [_tableView reloadData];
        
        NSString *infoString = [NSString stringWithFormat:@"%@ %@", [lastestForecast stringTemperature], [lastestForecast summary]];
        NSString *iconString = [NSString stringWithFormat:@"%@", [lastestForecast icon]];
        [_sharedDefaults setObject:infoString forKey:@"info"];
        [_sharedDefaults setObject:iconString forKey:@"icon"];
        [_sharedDefaults synchronize];
        
        [[self refreshControl] endRefreshing];
    }];
}

@end
