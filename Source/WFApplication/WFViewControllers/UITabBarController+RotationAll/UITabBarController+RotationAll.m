//
//  UITabBarController+RotationAll.m
//  weather
//
//  Created by Alex Nadein on 9/24/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "UITabBarController+RotationAll.h"

@implementation UITabBarController (RotationAll)

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end
