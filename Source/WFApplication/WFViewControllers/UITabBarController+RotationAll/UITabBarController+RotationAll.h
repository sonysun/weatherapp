//
//  UITabBarController+RotationAll.h
//  weather
//
//  Created by Alex Nadein on 9/24/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarController (RotationAll)

- (NSUInteger)supportedInterfaceOrientations;

@end
