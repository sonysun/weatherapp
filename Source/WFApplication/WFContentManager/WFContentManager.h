//
//  WFContentManager.h
//  weather
//
//  Created by Alex Nadein on 9/18/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WFParsedForecast.h"

@interface WFContentManager : NSObject

- (void)getContentWithBlock:(void (^)(WFParsedForecast *))block;

@end
