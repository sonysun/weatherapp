//
//  WFContentManager.m
//  weather
//
//  Created by Alex Nadein on 9/18/15.
//  Copyright © 2015 Alex Nadein. All rights reserved.
//

#import "WFContentManager.h"
#import "WFReachabilityManager.h"
#import "WFLocationDetector.h"
#import "WFWeatherForecaster.h"
#import "WFDataStorage.h"
#import "WFSQLiteDatabaseManager.h"

@interface WFContentManager ()

@property(nonatomic, strong) WFWeatherForecaster *weatherForecaster;
@property(nonatomic, strong) WFLocation *recentLocation;
@property(nonatomic, strong) WFLocationDetector *locationDetector;
@property(nonatomic, strong) WFReachabilityManager *reachabilityManager;
@property(nonatomic, strong) WFParsedForecast *forecast;
@property(nonatomic, strong) id<WFDAOProtocol> dataStorage;

- (void)storeForecast;

@end

@implementation WFContentManager

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        _reachabilityManager = [[WFReachabilityManager alloc] init];
        _locationDetector = [[WFLocationDetector alloc] init];
        _weatherForecaster = [[WFWeatherForecaster alloc] init];
        _dataStorage = [[WFSQLiteDatabaseManager alloc] init];
    }
    
    return self;
}

- (void)getContentWithBlock:(void (^)(WFParsedForecast *))block
{
    if ([_reachabilityManager netStatus] == NotReachable)
    {
        block([_dataStorage lastestForecast]);
    }
    else
    {
        [_locationDetector detectLocationWithBlock:^(WFLocation *location)
        {
            _recentLocation = location;
            [self storeForecast];
           
            [_dataStorage updateForecastPlace:[location placeName]];
            
            [_weatherForecaster weatherForecastForLocation:[self recentLocation] completionHandler:^(WFParsedForecast *parsedForecast)
            {
                [_dataStorage updateForecast:parsedForecast];
                [self setForecast:parsedForecast];
                [_forecast setPlace:[_recentLocation placeName]];
                block(_forecast);
            }];
        }];
    }
}

- (void)storeForecast
{
    [_dataStorage saveForecast:_forecast forLocation:_recentLocation];
}

@end
