//
//  XCTestMain.m
//  NIXProject
//
//  Created by Nezhelskoy Iliya on 11/09/14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import "XCTestAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XCTestAppDelegate class]));
    }
}